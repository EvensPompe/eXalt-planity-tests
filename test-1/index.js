const { S3 } = require("aws-sdk");
const s3 = new S3({ region: "eu-west-3" });
let response;

exports.handler = async (event, context) => {
  const bucketName = event.bucketName;
  const fileName = event.fileName;
  const fileContent = event.fileContent;
  const params = {
    Bucket: bucketName,
    Key: fileName,
    Body: fileContent,
  };
  try {
    response = await s3.putObject(params).promise();
  } catch (err) {
    console.log(err);
    return err;
  }
  return response;
};
