const { SQS, S3 } = require("aws-sdk");
const sqs = new SQS({ region: "eu-west-3" });
const s3 = new S3({ region: "eu-west-3" });
let response;
exports.handler = async (event, context) => {
  const bucketName = event.bucketName;
  const queueUrl = event.queueUrl;
  const sqsParams = {
    QueueUrl: queueUrl,
    MaxNumberOfMessages: 10,
  };
  let fileContent = "";
  let allEntries = [];
  const entriesIterator = new Object();
  entriesIterator[Symbol.asyncIterator] = async function* () {
    try {
      while (true) {
        let receiveMessages = await sqs.receiveMessage(sqsParams).promise();
        if (!receiveMessages.Messages) return;
        for (const message of receiveMessages.Messages) {
          yield {
            Body: `${message.Body}\n`,
            Id: message.MessageId,
            ReceiptHandle: message.ReceiptHandle,
          };
        }
      }
    } catch (error) {
      throw error;
    }
  };
  try {
    for await (const { Body, ...other } of entriesIterator) {
      const isRegistered = allEntries.some(
        (entry) => entry.MessageId === other.Id
      );
      if (!isRegistered) {
        fileContent += Body;
        allEntries.push({ ...other });
      }
    }
    const groupEntries = allEntries
      .map((_, index) => allEntries.slice(index * 10, (index + 1) * 10))
      .filter((entries) => entries.length > 0);
    for (const entries of groupEntries) {
      const responseDelete = await sqs
        .deleteMessageBatch({ QueueUrl: queueUrl, Entries: entries })
        .promise();
      console.log(JSON.stringify(responseDelete));
    }
    const date = new Date()
      .toLocaleDateString("fr", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
      })
      .split("/")
      .reverse()
      .join("-");
    const s3Params = {
      Bucket: bucketName,
      Key: `recrutement/${date}.jsonl`,
      Body: fileContent,
    };
    response = await s3.putObject(s3Params).promise();
  } catch (err) {
    console.log(err);
    throw err;
  }
  return response;
};
