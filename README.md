**TESTS Planity**

INSTRUCTIONS

- Aller dans le test-1 ou test-2 executant la commande `cd test-1` ou `cd test-2`.
- Remplir les valeurs dans le fichier `event.json`.

- Pour le test-1 :
        - "bucketName": nom du bucket.
        - "fileName": chemin vers le fichier.
        - "fileContent": le contenu du fichier.

- Pour le test-2 :
        - "queueUrl": l'url de la queue du AWS SQS.
        - "bucketName" : nom du bucket.

- Lancer la commande `npm run start`.